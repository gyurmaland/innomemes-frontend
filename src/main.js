import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import { store } from './store'
import Vuetify from 'vuetify'
import 'animate.css'
import VueClipboard from 'vue-clipboard2'
import '@mdi/font/css/materialdesignicons.css'
import VueFlashMessage from 'vue-flash-message'

Vue.config.productionTip = false
Vue.use(VueClipboard)
Vue.use(Vuetify, {
  iconfont: 'mdi'
})
Vue.use(VueFlashMessage)
require('vue-flash-message/dist/vue-flash-message.min.css')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
