import _ from 'lodash'

export function isTwoArraysOfObjectsEquals (array1, array2, fieldName) {
  let uniqueResultOne = _.differenceBy(array1, array2, fieldName)
  let uniqueResultTwo = _.differenceBy(array2, array1, fieldName)

  let result = uniqueResultOne.concat(uniqueResultTwo)
  if (result.length > 0) {
    return false
  }
  return true
}
