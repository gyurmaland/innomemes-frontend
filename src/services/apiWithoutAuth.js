import axios from 'axios'

export default() => {
  return axios.create({
    // baseURL: `http://localhost:8080`,
    // baseURL: `https://innomemes-backend.herokuapp.com`,
    // baseURL: `https://mem-backend.service.innovitech.internal`,
    baseURL: `https://mem-backend.innovitech.hu`,
    withCredentials: false,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
}
