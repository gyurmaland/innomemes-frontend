import axios from 'axios'
import { store } from '../store'
import router from '../router'

export default() => {
  let axiosStuff
  axiosStuff = axios.create({
    // baseURL: `http://localhost:8080`,
    // baseURL: `https://innomemes-backend.herokuapp.com`,
    // baseURL: `https://mem-backend.service.innovitech.internal`,
    baseURL: `https://mem-backend.innovitech.hu`,
    withCredentials: false,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + store.state.auth.authToken
    }
  })

  axiosStuff.interceptors.response.use((response) => { // intercept the global error
    return response
  }, function (error) {
    if (error.response.status === 401) {
      store.dispatch('logoutUser', {})
      router.push('/login')
    }
    // Do something with response error
    return Promise.reject(error)
  })

  return axiosStuff
}
