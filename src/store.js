import Vue from 'vue'
import Vuex from 'vuex'
import base from './modules/base'
import auth from './modules/auth'
import meme from './modules/meme'
import memeSearch from './modules/memeSearch'
import like from './modules/like'
import userRank from './modules/userRank'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    base,
    auth,
    meme,
    memeSearch,
    like,
    userRank
  },
  plugins: [createPersistedState()]
})
