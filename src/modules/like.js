import Api from '@/services/api'

const getDefaultState = () => {
  return {
  }
}

// initial state
const state = getDefaultState()

const getters = {
}

const mutations = {
}

const actions = {
  saveOrDeleteLike ({ commit }, likeData) {
    Api().post('/like/save', {
      memeId: likeData.memeId,
      userId: likeData.userId
    })
      .then(res => {
      })
      .catch(error => console.log(error))
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
