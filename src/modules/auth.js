import Api from '@/services/api'
import ApiWithoutAuth from '../services/apiWithoutAuth'

const getDefaultState = () => {
  return {
    emailSentAlert: false,
    isUsernameAlreadyTaken: false,
    isEmailAlreadyTaken: false,
    authToken: null,
    expireTime: null,
    secondsTillExpire: null,
    loggedInUser: {
      cityOffices: '',
      email: '',
      fullName: '',
      id: '',
      projects: '',
      username: ''
    }
  }
}

// initial state
const state = getDefaultState()

const getters = {}

const mutations = {
  SET_EMAIL_SENT_ALERT (state, emailSentAlert) {
    state.emailSentAlert = emailSentAlert
  },
  SET_USERNAME_ALREADY_TAKEN (state, isUsernameAlreadyTaken) {
    state.isUsernameAlreadyTaken = isUsernameAlreadyTaken
  },
  SET_EMAIL_ALREADY_TAKEN (state, isEmailAlreadyTaken) {
    state.isEmailAlreadyTaken = isEmailAlreadyTaken
  },
  SET_AUTH_TOKEN (state, response) {
    state.authToken = response.data.accessToken
  },
  SET_LOGGED_IN_USER (state, response) {
    state.loggedInUser = response
  },
  SET_EXPIRY_DATE (state, date) {
    state.expireTime = date
  },
  SET_SECONDS_TILL_EXPIRE (state, seconds) {
    state.secondsTillExpire = seconds
  },
  SET_USER_NEW_CITY_OFFICES (state, cityOffices) {
    state.loggedInUser.cityOffices = cityOffices
  },
  REMOVE_USER_CITY_OFFICE (state, item) {
    state.loggedInUser.cityOffices.splice(state.loggedInUser.cityOffices.indexOf(item), 1)
    state.loggedInUser.cityOffices = [...state.loggedInUser.cityOffices]
  },
  SET_USER_NEW_PROJECTS (state, projects) {
    state.loggedInUser.projects = projects
  },
  REMOVE_USER_PROJECT (state, item) {
    state.loggedInUser.projects.splice(state.loggedInUser.projects.indexOf(item), 1)
    state.loggedInUser.projects = [...state.loggedInUser.projects]
  },
  RESET_AUTH_STATE (state) {
    Object.assign(state, getDefaultState())
  }
}

const actions = {
  registerUser ({ commit }, userData) {
    userData.vm.isDisabled = true
    ApiWithoutAuth().post('/auth/signup', {
      fullName: userData.fullName,
      username: userData.username,
      email: userData.email,
      password: userData.password,
      cityOfficeNames: userData.cityOfficeList,
      projectNames: userData.projectList
    })
      .then(res => {
        commit('SET_EMAIL_SENT_ALERT', true)
        userData.vm.$router.push('/login')
      })
      .catch(error => console.log(error))
  },
  resetEmailAlert ({ commit }) {
    commit('SET_EMAIL_SENT_ALERT', false)
  },
  isUsernameAlreadyTaken ({ commit }, username) {
    ApiWithoutAuth().get('/auth/isUsernameAlreadyTaken/' + username.username)
      .then(res => {
        commit('SET_USERNAME_ALREADY_TAKEN', res.data)
      })
      .catch(error => console.log(error))
  },
  isEmailAlreadyTaken ({ commit }, email) {
    ApiWithoutAuth().get('/auth/isEmailAlreadyTaken/' + email.email)
      .then(res => {
        commit('SET_EMAIL_ALREADY_TAKEN', res.data)
      })
      .catch(error => console.log(error))
  },
  activateUser ({ commit }, activationData) {
    ApiWithoutAuth().get('/auth/activation/' + activationData.code, {})
      .then(res => {
        activationData.vm.activationSuccessAlert = true
      })
      .catch(error => {
        activationData.vm.activationFailAlert = true
      })
  },
  loginUser ({ commit }, userData) {
    userData.vm.isDisabled = true
    ApiWithoutAuth().post('/auth/signin', {
      username: userData.username,
      password: userData.password
    })
      .then(res => {
        commit('SET_AUTH_TOKEN', res)
        commit('SET_LOGGED_IN_USER', res.data.userResponse)
        let d = new Date()
        commit('SET_EXPIRY_DATE', new Date(d.getTime() + 86400000))
        commit('SET_SECONDS_TILL_EXPIRE', (new Date(d.getTime()) - new Date()) / 1000)
        userData.vm.$router.push('/memes')
        userData.vm.isDisabled = false
      })
      .catch(error => {
        if (error.response.status === 401) {
          userData.vm.loginFailAlert = true
          userData.vm.isDisabled = false
        }
        if (error.response.status === 404) {
          userData.vm.loginNotYetActivatedFailAlert = true
          userData.vm.isDisabled = false
        }
      })
  },
  refreshToken ({ commit }) {
    Api().get('/auth/refreshToken', {})
      .then(res => {
        commit('SET_AUTH_TOKEN', res)
        let d = new Date()
        commit('SET_EXPIRY_DATE', new Date(d.getTime() + 86400000))
        commit('SET_SECONDS_TILL_EXPIRE', (new Date(d.getTime()) - new Date()) / 1000)
      })
      .catch(error => {
        console.log(error)
      })
  },
  generatePasswordResetCode ({ commit }, passwordResetData) {
    passwordResetData.vm.isSubmitDisabled = true
    ApiWithoutAuth().post('/auth/passwordReset/generateCode', {
      email: passwordResetData.email
    })
      .then(res => {
        passwordResetData.vm.resetLinkSentAlert = true
        passwordResetData.vm.isSubmitDisabled = false
      })
      .catch(error => {
        passwordResetData.vm.isSubmitDisabled = false
        console.log(error)
      })
  },
  checkIfResetCodeCorrect ({ commit }, passwordResetData) {
    ApiWithoutAuth().get('/auth/checkPwResetCode/' + passwordResetData.passwordResetCode)
      .then(res => {

      })
      .catch(error => {
        passwordResetData.vm.passwordResetTokenIncorrect = true
        passwordResetData.vm.isFormVisible = false
      })
  },
  changePassword ({ commit }, passwordResetData) {
    passwordResetData.vm.isSubmitDisabled = true
    ApiWithoutAuth().post('/auth/passwordReset/' + passwordResetData.passwordResetCode, {
      password: passwordResetData.password
    })
      .then(res => {
        passwordResetData.vm.isFormVisible = false
        passwordResetData.vm.passwordChanged = true
        passwordResetData.vm.isSubmitDisabled = false
      })
      .catch(error => {
        passwordResetData.isSubmitDisabled = false
        console.log(error)
      })
  },
  setUserNewCityOffices ({ commit }, cityOfficesData) {
    commit('SET_USER_NEW_CITY_OFFICES', cityOfficesData)
  },
  removeUserCityOffice ({ commit }, item) {
    commit('REMOVE_USER_CITY_OFFICE', item)
  },
  setUserNewProjects ({ commit }, projectsData) {
    commit('SET_USER_NEW_PROJECTS', projectsData)
  },
  removeUserProject ({ commit }, item) {
    commit('REMOVE_USER_PROJECT', item)
  },
  getLoggedInUser ({ commit }, userToken) {
    Api().post('/auth/loggedInUser', {
      token: userToken.userToken
    })
      .then(res => {
        commit('SET_LOGGED_IN_USER', res.data)
      })
      .catch(
        error => console.log(error))
  },
  updateUser ({ commit }, userPreferences) {
    Api().post('/user/updatePreferences', {
      cityOfficeNames: userPreferences.cityOfficeList.map(co => co.name),
      projectNames: userPreferences.projectList.map(p => p.name),
      userId: userPreferences.userId
    })
      .then(res => {
      })
      .catch(
        error => console.log(error))
  },
  convertExpireTimeToDate ({ commit }, expireTime) {
    commit('SET_EXPIRY_DATE', new Date(expireTime))
  },
  updateSecondsTillExpire ({ commit }, expireTime) {
    if (expireTime !== null && expireTime !== undefined) {
      commit('SET_SECONDS_TILL_EXPIRE', (new Date(expireTime.getTime()) - new Date()) / 1000)
    }
  },
  logoutUser ({ commit }) {
    commit('RESET_AUTH_STATE')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
