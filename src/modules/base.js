const getDefaultState = () => {
  return {
    projects: [
      {
        id: 1,
        name: 'BICON'
      },
      {
        id: 3,
        name: 'CTA'
      },
      {
        id: 4,
        name: 'GENAT'
      },
      {
        id: 2,
        name: 'OSZ'
      },
      {
        id: 35,
        name: 'TÉM'
      }
    ],
    cityOffices: [
      {
        id: 5,
        name: 'Eger'
      },
      {
        id: 6,
        name: 'Debrecen'
      },
      {
        id: 7,
        name: 'Budapest'
      }
    ]
  }
}

// initial state
const state = getDefaultState()

const getters = {
}

const mutations = {
  RESET_BASE_STATE (state) {
    Object.assign(state, getDefaultState())
  }
}

const actions = {
  resetBaseState ({ commit }, userData) {
    commit('RESET_BASE_STATE')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
