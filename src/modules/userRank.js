import Api from '@/services/api'

const state = {
  top3User: []
}

const getters = {

}

const mutations = {
  SET_TOP3_USER (state, top3UserData) {
    state.top3User = top3UserData
  }
}

const actions = {
  getTop3User ({ commit }, userToken) {
    Api().get('/user/top3user', {
    })
      .then(res => {
        commit('SET_TOP3_USER', res.data)
      })
      .catch(
        error => console.log(error))
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
