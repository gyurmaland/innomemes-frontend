const state = {
  universalSearchStr: '',
  orderBy: 'createdDate',
  universalCityOffice: false,
  cityOffice: [],
  universalProject: false,
  project: []
}

const getters = {
  GET_CITY_OFFICE (state) {
    return state.cityOffice
  },
  GET_PROJECT (state) {
    return state.project
  }
}

const mutations = {
  SET_UNIVERSAL_SEARCH_STR (state, universalSearchStr) {
    state.universalSearchStr = universalSearchStr
  },
  SET_MEME_ORDER_BY (state, orderBy) {
    state.orderBy = orderBy
  },
  SET_UNIVERSAL_CITY_OFFICE (state, universalCityOffice) {
    state.universalCityOffice = universalCityOffice
  },
  SET_CITY_OFFICE (state, cityOffice) {
    state.cityOffice = cityOffice
  },
  SET_UNIVERSAL_PROJECT (state, universalProject) {
    state.universalProject = universalProject
  },
  SET_PROJECT (state, project) {
    state.project = project
  },
  REMOVE_CITY_OFFICE (state, item) {
    state.cityOffice.splice(state.cityOffice.indexOf(item), 1)
    state.cityOffice = [...state.cityOffice]
  },
  REMOVE_PROJECT (state, item) {
    state.project.splice(state.project.indexOf(item), 1)
    state.project = [...state.project]
  }
}

const actions = {
  setMemeUniversalSearchStr ({ commit }, memeSearchUniversalStr) {
    commit('SET_UNIVERSAL_SEARCH_STR', memeSearchUniversalStr.search)
  },
  setMemeOrderBy ({ commit }, orderBy) {
    commit('SET_MEME_ORDER_BY', orderBy.orderBy)
  },
  setMemeUniversalCityOffice ({ commit }, universalSearchCityOffice) {
    commit('SET_UNIVERSAL_CITY_OFFICE', universalSearchCityOffice.search)
  },
  setMemeCityOfficeSearch ({ commit }, memeSearchCityOffice) {
    commit('SET_CITY_OFFICE', memeSearchCityOffice.search)
  },
  setMemeUniversalProject ({ commit }, universalProject) {
    commit('SET_UNIVERSAL_PROJECT', universalProject.search)
  },
  setMemeProjectSearch ({ commit }, memeSearchProject) {
    commit('SET_PROJECT', memeSearchProject.search)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
