import Api from '@/services/api'

const getDefaultState = () => {
  return {
    memeList: [],
    meme: {
      user: {
        username: ''
      },
      likes: [],
      projects: [],
      cityOffices: []
    },
    memeSuccessAlert: false
  }
}

// initial state
const state = getDefaultState()

const getters = {
}

const mutations = {
  SET_MEME (state, meme) {
    state.meme = meme
  },
  SET_MEME_SUCCESS (state, memeSuccess) {
    state.memeSuccessAlert = memeSuccess
  },
  RESET_MEME_STATE (state) {
    Object.assign(state, getDefaultState())
  }
}

const actions = {
  getMemeById ({ commit }, memeData) {
    Api().get('/memes/' + memeData.memeId, {
    }).then(res => {
      commit('SET_MEME', res.data)
    })
  },
  uploadMeme ({ commit }, memeData) {
    memeData.vm.isDisabled = true
    Api().post('/meme/save', {
      name: memeData.name,
      projectNames: memeData.projectNames,
      cityOfficeNames: memeData.cityOfficeNames,
      picUrl: memeData.picUrl
    })
      .then(res => {
        commit('SET_MEME_SUCCESS', true)
        memeData.router.push('/memes/' + res.data)
        memeData.vm.isDisabled = false
      })
      .catch(error => {
        console.log(error)
        memeData.vm.isDisabled = false
      })
  },
  resetMemeState ({ commit }) {
    commit('RESET_MEME_STATE')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
