import Vue from 'vue'
import Router from 'vue-router'
import SignUp from './components/auth/SignUp'
import Login from './components/auth/Login'
import ResetPasswordClaim from './components/auth/RestPasswordClaim'
import NewPassword from './components/auth/NewPassword'
import MemeList from './components/meme/MemeList'
import NewMeme from './components/meme/NewMeme'
import UserRank from './components/rank/UserRank'
import MemeDetails from './components/meme/MemeDetails'
import MyProfile from './components/auth/MyProfile'
import { store } from './store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/memes'
    },
    {
      path: '/signUp',
      name: 'signUp',
      component: SignUp
    },
    {
      path: '/activation/:code',
      name: 'activationSignIn',
      component: Login
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/resetPasswordClaim',
      name: 'resetPasswordClaim',
      component: ResetPasswordClaim
    },
    {
      path: '/newPassword/:code',
      name: 'newPassword',
      component: NewPassword
    },
    {
      path: '/myProfile',
      name: 'myProfile',
      component: MyProfile
    },
    {
      path: '/memes',
      name: 'memeList',
      component: MemeList
    },
    {
      path: '/uploadMeme',
      name: 'newMeme',
      component: NewMeme
    },
    {
      path: '/ranks',
      name: 'userRank',
      component: UserRank
    },
    {
      path: '/memes/:memeId',
      name: 'memeDetails',
      component: MemeDetails
    },
    {
      path: '*',
      component: MemeList
    }
  ]
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/signUp', '/resetPasswordClaim']
  const regexPublicPage = new RegExp('/activation/*')
  const regexPublicPage2 = new RegExp('/newPassword/*')
  const authRegex = !regexPublicPage.test(to.path)
  const authRegex2 = !regexPublicPage2.test(to.path)
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = store.state.auth.authToken !== null

  if (authRequired && authRegex && authRegex2 && !loggedIn) {
    return next('/login')
  }

  if (to.path === '/login' && loggedIn) {
    return next('/memes')
  }

  next()
})

export default router
